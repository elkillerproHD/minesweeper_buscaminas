import React from 'react';
import {ScrollView, Alert, AsyncStorage, ToastAndroid} from 'react-native';
import {
  CellContainer,
  Board,
  RowOfCells,
  SubContainer,
  Container,
} from './styles';
import {Cell} from '../../components/cell';
import {Constants} from '../../constants';
import {Header} from '../../components/header';
import {Footer} from '../../components/footer';
import {Modal} from '../../components/modal';

class Game extends React.Component {
  PreStartGame = (state) => {
    this.grid = Array.apply(null, Array(state.colNumber)).map(() => {
      return Array.apply(null, Array(state.rowNumber)).map(() => {
        return null;
      });
    });
    this.setState({
      cellsData: [],
      renderColsAndRows: [],
      gameOver: false,
      winGame: false,
      zoom: 0,
      mineIconActivated: false,
      questionIconActivated: false,
      minesInGame: 0,
      minesIconsUsedQuantity: 0,
      minesIconsUsedCoords: [],
      timeLapse: 0,
      isRendered: false,
      somethingChangedForRestart: false
    });
  };
  constructor(props) {
    super(props);
    this.grid = Array.apply(null, Array(Constants.hardcodeCol)).map(() => {
      return Array.apply(null, Array(Constants.hardcodeRow)).map((_) => {
        return null;
      });
    });
    this.state = {
      cellsData: [],
      renderColsAndRows: [],
      somethingChangedForRestart: false,
      gameOver: false,
      winGame: false,
      modalPage: false,
      zoom: 0,
      mineIconActivated: false,
      questionIconActivated: false,
      minesInGame: 0,
      minesIconsUsedQuantity: 0,
      minesIconsUsedCoords: [],
      timeLapse: 0,
      isMineConstant: Constants.isMineConstant,
      colNumber: Constants.hardcodeCol,
      rowNumber: Constants.hardcodeRow,
      isRendered: false,
    };
  }
  SaveGame = async () => {
    try {
      await AsyncStorage.setItem(
        'MyOnlySavedGame <3',
        JSON.stringify(this.state),
      );
      ToastAndroid.show('Game Saved', ToastAndroid.SHORT);
    } catch (error) {
      // Error saving data
    }
  };
  LoadGame = async () => {
    try {
      const value = await AsyncStorage.getItem('MyOnlySavedGame <3');
      if (value !== null) {
        // We have data!!
        const json = JSON.parse(value);
        ToastAndroid.show('Game Loaded', ToastAndroid.SHORT);
        clearInterval(this.timeLapse);
        this.grid = Array.apply(null, Array(json.colNumber)).map(() => {
          return Array.apply(null, Array(json.rowNumber)).map(() => {
            return null;
          });
        });
        this.setState(json);
        setTimeout(this.StartGame, 100);
      }
    } catch (error) {
      // Error retrieving data
    }
  };
  QuestionMarkOnPress = () => {
    const {state} = this;
    this.setState(
      {
        questionIconActivated: !state.questionIconActivated,
        mineIconActivated: false,
      },
      () =>
        setTimeout(() => {
          this.RenderCells();
        }, 100),
    );
  };
  MineIconOnPress = () => {
    const {state} = this;
    this.setState(
      {
        mineIconActivated: !state.mineIconActivated,
        questionIconActivated: false,
      },
      () =>
        setTimeout(() => {
          this.RenderCells();
        }, 100),
    );
  };
  AddQuestionMarkToCell = (col, row) => {
    let {cellsData, minesIconsUsedQuantity} = this.state;
    cellsData[col][row].questionMarkIcon = !cellsData[col][row]
      .questionMarkIcon;
    if (cellsData[col][row].mineIcon) {
      minesIconsUsedQuantity--;
    }
    cellsData[col][row].mineIcon = false;
    this.setState(
      {
        cellsData,
        minesIconsUsedQuantity,
      },
      () =>
        setTimeout(() => {
          this.RenderCells();
        }, 100),
    );
  };
  WinGame = () => {
    Alert.alert(
      'Wow, you win, i programmed it but i never can do it. Ty for playing',
    );
    this.setState({
      winGame: true,
      gameOver: true,
    });
  };
  AddMineIconToCell = (col, row, isMine) => {
    let {
      cellsData,
      minesIconsUsedQuantity,
      minesIconsUsedCoords,
      minesInGame,
    } = this.state;
    let allMinesDiscovered = false;
    cellsData[col][row].questionMarkIcon = false;
    if (cellsData[col][row].mineIcon) {
      minesIconsUsedQuantity--;
      minesIconsUsedCoords.map((val, index) => {
        if (val.x === col && val.y === row) {
          minesIconsUsedCoords.splice(index, 1);
        }
      });
    } else {
      minesIconsUsedQuantity++;
      if (minesIconsUsedCoords.length < minesInGame) {
        minesIconsUsedCoords.push({
          x: col,
          y: row,
          isMine,
        });
      }
      if (minesIconsUsedCoords.length === minesInGame) {
        minesIconsUsedCoords.map((val) => {
          allMinesDiscovered = true;
          if (!val.isMine) {
            allMinesDiscovered = false;
          }
        });
      }
    }
    cellsData[col][row].mineIcon = !cellsData[col][row].mineIcon;
    this.setState(
      {
        cellsData,
        minesIconsUsedQuantity,
        minesIconsUsedCoords,
      },
      () =>
        setTimeout(() => {
          this.RenderCells();
          if (allMinesDiscovered) {
            this.WinGame();
          }
        }, 100),
    );
  };
  AddZoom = () => {
    const {state} = this;
    if (state.zoom < Constants.maxZoom) {
      this.setState(
        {
          zoom: state.zoom + Constants.zoomAddedLvl,
        },
        () =>
          setTimeout(() => {
            this.RenderCells();
          }, 100),
      );
    }
  };
  RemoveZoom = () => {
    const {state} = this;
    if (state.zoom > 0) {
      this.setState(
        {
          zoom: state.zoom - Constants.zoomAddedLvl,
        },
        () =>
          setTimeout(() => {
            this.RenderCells();
          }, 100),
      );
    }
  };
  onDie = () => {
    let {cellsData} = this.state;
    this.setState({
      gameOver: true,
    });
    for (let i = 0; i < Constants.hardcodeCol; i++) {
      for (let j = 0; j < Constants.hardcodeRow; j++) {
        if (cellsData[i][j].isMine) {
          cellsData[i][j].open = true;
        }
      }
    }
    this.setState({
      cellsData,
    });
  };
  StartGame = () => {
    this.RenderCells();
    this.setState({
      isRendered: true,
    });
    this.timeLapse = setInterval(() => {
      this.setState({
        timeLapse: this.state.timeLapse + 1,
      });
    }, 1000);
  };
  componentDidMount() {
    this.StartGame();
  }
  revealNeighbors = (x, y) => {
    const {cellsData, gameOver} = this.state;

    for (let i = -1; i <= 1; i++) {
      for (let j = -1; j <= 1; j++) {
        if (
          (i !== 0 || j !== 0) &&
          x + i >= 0 &&
          x + i <= Constants.hardcodeCol - 1 &&
          y + j >= 0 &&
          y + j <= Constants.hardcodeRow - 1
        ) {
        }
        if (
          (i !== 0 || j !== 0) &&
          x + i >= 0 &&
          x + i <= Constants.hardcodeCol - 1 &&
          y + j >= 0 &&
          y + j <= Constants.hardcodeRow - 1 &&
          !gameOver &&
          !cellsData[x + i][y + j].open &&
          !cellsData[x + i][y + j].isMine
        ) {
          this.onReveal(x + i, y + j);
        }
      }
    }
  };
  onReveal = (x, y) => {
    let {cellsData} = this.state;
    cellsData[x][y].open = true;
    this.setState(
      {
        cellsData,
      },
      () => {
        let neighbors = 0;
        for (let i = -1; i <= 1; i++) {
          for (let j = -1; j <= 1; j++) {
            if (
              x + i >= 0 &&
              x + i <= Constants.hardcodeCol - 1 &&
              y + j >= 0 &&
              y + j <= Constants.hardcodeRow - 1
            ) {
              if (cellsData[x + i][y + j].isMine) {
                neighbors++;
              }
            }
          }
        }
        this.setState(
          {
            cellsData,
          },
          () => {
            if (neighbors) {
              cellsData[x][y].neighbors = neighbors;
              this.RenderCells();
            } else {
              this.revealNeighbors(x, y);
              this.RenderCells();
            }
          },
        );
      },
    );
  };
  RedMineCellOnPress = (col, row) => {
    const {state} = this;
    state.cellsData[col][row].mineCellPressed = true;
    this.onDie();
    this.RenderCells();
  };
  RenderCells = () => {
    let renderColsAndRows = [];
    let cellList = [];
    let dataOfCellList = [];
    let cellsData = [];
    let colIdx = 0;
    let rowIdx = 0;
    let totalMines = 0;
    const gridRender = this.grid;
    // eslint-disable-next-line no-unused-vars
    for (let colx of gridRender) {
      // eslint-disable-next-line no-unused-vars
      for (let rowx of gridRender[colIdx]) {
        let isMine,
          open,
          neighbors,
          mineCellPressed,
          mineIcon,
          questionMarkIcon;
        if (this.state.isRendered) {
          isMine = this.state.cellsData[colIdx][rowIdx].isMine;
          if (isMine) {
            totalMines++;
          }
          open = this.state.cellsData[colIdx][rowIdx].open;
          neighbors = this.state.cellsData[colIdx][rowIdx].neighbors;
          mineCellPressed = this.state.cellsData[colIdx][rowIdx]
            .mineCellPressed;
          mineIcon = this.state.cellsData[colIdx][rowIdx].mineIcon;
          questionMarkIcon = this.state.cellsData[colIdx][rowIdx]
            .questionMarkIcon;
        } else {
          isMine = Math.random() < this.state.isMineConstant;
          if (isMine) {
            totalMines++;
          }
          open = false;
          neighbors = null;
          mineCellPressed = false;
          mineIcon = false;
          questionMarkIcon = false;
        }

        let cellComponent;
        if (colIdx === 0 && rowIdx === 0) {
        }
        cellComponent = (
          <Cell
            isMine={isMine}
            open={open}
            neighbors={neighbors}
            mineCellPressed={mineCellPressed}
            mineIcon={mineIcon}
            questionMarkIcon={questionMarkIcon}
            mineIconActivated={this.state.mineIconActivated}
            questionIconActivated={this.state.questionIconActivated}
            zoom={this.state.zoom}
            isMineConstant={this.state.isMineConstant}
            gameOver={this.state.gameOver}
            RedMineCellOnPress={this.RedMineCellOnPress}
            QuestionMarkOnPress={this.QuestionMarkOnPress}
            MineIconOnPress={this.MineIconOnPress}
            AddQuestionMarkToCell={this.AddQuestionMarkToCell}
            AddMineIconToCell={this.AddMineIconToCell}
            onReveal={this.onReveal}
            onDie={this.onDie}
            key={`col:${colIdx}row:${rowIdx}`}
            x={colIdx}
            y={rowIdx}
          />
        );
        cellList.push(cellComponent);
        dataOfCellList.push({
          isMine,
          open,
          neighbors,
          mineCellPressed,
          mineIcon,
          questionMarkIcon,
        });
        rowIdx++;
      }
      rowIdx = 0;
      renderColsAndRows.push(
        <RowOfCells
          rowNumber={this.state.rowNumber}
          zoom={this.state.zoom}
          key={colIdx}>
          {cellList}
        </RowOfCells>,
      );
      cellsData.push(dataOfCellList);
      cellList = [];
      dataOfCellList = [];
      colIdx++;
    }
    this.setState({
      minesInGame: totalMines,
      renderColsAndRows,
      cellsData,
    });
  };
  ResetGame = () => {
    clearInterval(this.timeLapse);
    this.PreStartGame(this.state);
    setTimeout(this.StartGame, 100);
  };
  OpenCloseModal = () => {
    const actualState = this.state.modalPage;
    this.setState({
      modalPage: !actualState,
    });

    if (actualState && this.state.somethingChangedForRestart) {
      setTimeout(this.ResetGame, 100);
    }
  };
  OnChangeCol = (colNumber) => {
    this.setState({
      // eslint-disable-next-line radix
      colNumber: parseInt(colNumber),
      somethingChangedForRestart: true,
    });
  };
  OnChangeRow = (rowNumber) => {
    this.setState({
      // eslint-disable-next-line radix
      rowNumber: parseInt(rowNumber),
      somethingChangedForRestart: true,
    });
  };
  SetDifficulty = (difficulty) => {
    if (difficulty === 'easy') {
      this.setState({
        isMineConstant: 0.15,
        somethingChangedForRestart: true,
      });
    } else if (difficulty === 'normal') {
      this.setState({
        isMineConstant: 0.25,
        somethingChangedForRestart: true,
      });
    } else if (difficulty === 'hard') {
      this.setState({
        isMineConstant: 0.35,
        somethingChangedForRestart: true,
      });
    }
  };

  render() {
    const {state} = this;
    if (state.modalPage) {
      return (
        <Modal
          SetDifficulty={this.SetDifficulty}
          OpenCloseModal={this.OpenCloseModal}
          colNumber={state.colNumber}
          rowNumber={state.rowNumber}
          OnChangeCol={this.OnChangeCol}
          OnChangeRow={this.OnChangeRow}
          SaveGame={this.SaveGame}
          LoadGame={this.LoadGame}
        />
      );
    }
    return (
      <Container>
        <Header
          winGame={state.winGame}
          minesInGame={state.minesInGame}
          minesIconsUsedQuantity={state.minesIconsUsedQuantity}
          timeLapse={state.timeLapse}
          reset={this.ResetGame}
          gameOver={state.gameOver}
        />
        <Board>
          <ScrollView>
            <ScrollView horizontal>
              <SubContainer
                colNumber={state.colNumber}
                rowNumber={state.rowNumber}
                zoom={state.zoom}>
                <CellContainer>{state.renderColsAndRows}</CellContainer>
              </SubContainer>
            </ScrollView>
          </ScrollView>
        </Board>
        <Footer
          QuestionMarkOnPress={this.QuestionMarkOnPress}
          MineIconOnPress={this.MineIconOnPress}
          mineIconActivated={state.mineIconActivated}
          questionIconActivated={state.questionIconActivated}
          AddZoom={this.AddZoom}
          RemoveZoom={this.RemoveZoom}
          openCloseModal={this.OpenCloseModal}
        />
      </Container>
    );
  }
}

export default Game;
