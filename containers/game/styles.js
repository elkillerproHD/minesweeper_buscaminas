import styled from 'styled-components';
import {Constants} from '../../constants';

export const Container = styled.SafeAreaView`
  background-color: #bdbdbd;
  width: ${Constants.width};
  height: ${Constants.height};
  justify-content: flex-start;
  align-items: center;
`;

export const Board = styled.View`
  width: ${Constants.width};
  height: ${Constants.height * 0.67};
  padding: 5%;
  justify-content: center;
  align-items: center;
  overflow: hidden;
  border-width: 8;
  border-top-color: #ffffff;
  border-left-color: #ffffff;
  border-right-color: #7d7d7d;
  border-bottom-color: #7d7d7d;
`;
export const SubContainer = styled.View`
  width: ${({zoom, rowNumber}) =>
    rowNumber * (Constants.cellSize * (1 + zoom / 10))};
  height: ${({zoom, colNumber}) =>
    colNumber * (Constants.cellSize * (1 + zoom / 10))};
  justify-content: center;
  align-items: center;
`;
export const CellContainer = styled.View`
  width: ${Constants.width};
  height: ${Constants.height};
  justify-content: center;
  align-items: center;
`;

export const RowOfCells = styled.View`
  width: ${({zoom, rowNumber}) =>
    rowNumber * (Constants.cellSize * (1 + zoom / 10))};
  height: ${({zoom}) => Constants.cellSize * (1 + zoom / 10)};
  flex-direction: row;
`;
