import React from 'react';
import {TouchableOpacity} from 'react-native';
import styled from 'styled-components';
import {Constants} from '../../constants';
import CrossImg from '../../images/close.png';

const Circle = {
  position: 'absolute',
  elevation: 2,
  zIndex: 2,
  width: Number.parseInt(Constants.width * 0.15),
  height: Number.parseInt(Constants.width * 0.15),
  borderRadius: Number.parseInt(Constants.width * 0.15),
  top: Number.parseInt(Constants.height * 0.77),
  right: Number.parseInt(Constants.width * 0.1),
  backgroundColor: '#fff',
  justifyContent: 'center',
  alignItems: 'center',
};
const ImgStyle = styled.Image`
  width: ${Number.parseInt(Constants.height * 0.1 - 50)};
  height: ${Number.parseInt(Constants.height * 0.1 - 50)};
`;
export const Close = (props) => {
  return (
    <TouchableOpacity style={Circle} onPress={props.onPress}>
      <ImgStyle source={CrossImg} />
    </TouchableOpacity>
  );
};
