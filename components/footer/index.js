import React from 'react';
import styled from 'styled-components';
import MenuImg from '../../images/menu.png';
import PlusImg from '../../images/plus.png';
import MinusImg from '../../images/minus.png';
import ZoomImg from '../../images/zoom.png';
import MineImg from '../../images/mine.png';
import QuesMarkImg from '../../images/question.png';
import {Constants} from '../../constants';

const Container = styled.View `
  width: ${Constants.width};
  height: ${Constants.height * 0.10};
  margin-top: ${Constants.height * 0.01};
  background-color: #bdbdbd;
  border-width: 8;
  border-bottom-color: #ffffff;
  border-right-color: #ffffff;
  border-left-color: #7d7d7d;
  border-top-color: #7d7d7d;
  justify-content: space-around;
  align-items: center;
  flex-direction: row;
`;

const Btn = styled.TouchableOpacity `
  width: ${Constants.width * 0.15};
  height: ${Constants.height * 0.10 - 26};
  background-color: #bdbdbd;
  border-width: 6;
  border-top-color: #ffffff;
  border-left-color: #ffffff;
  border-right-color: #7d7d7d;
  border-bottom-color: #7d7d7d;
  justify-content: center;
  align-items: center;
  flex-direction: row;
`
const BtnFocus = styled.TouchableOpacity `
  width: ${Constants.width * 0.15};
  height: ${Constants.height * 0.10 - 26};
  background-color: #ffdf00;
  border-width: 6;
  border-top-color: #ffffff;
  border-left-color: #ffffff;
  border-right-color: #7d7d7d;
  border-bottom-color: #7d7d7d;
  justify-content: center;
  align-items: center;
  flex-direction: row;
`

const ImgStyle = styled.Image `
  width: ${Constants.width * 0.07};
  height: ${Constants.width * 0.07};
`
const ImgStyle2 = styled.Image `
  width: ${Constants.width * 0.04};
  height: ${Constants.width * 0.04};
`
const SpaceView = styled.View `
  width: ${Constants.width * 0.02};
  height: ${Constants.height * 0.10 - 26};
`

export const Footer = (props) => {
  return(
    <Container>
      <Btn onPress={props.openCloseModal}>
        <ImgStyle source={MenuImg} />
      </Btn>

      <Btn onPress={props.RemoveZoom}>
        <ImgStyle2 source={ZoomImg} />
        <SpaceView />
        <ImgStyle2 source={MinusImg} />
      </Btn>
      <Btn onPress={props.AddZoom}>
        <ImgStyle2 source={ZoomImg} />
        <SpaceView />
        <ImgStyle2 source={PlusImg} />
      </Btn>
      {
        props.mineIconActivated
        ? (
            <BtnFocus onPress={props.MineIconOnPress}>
              <ImgStyle source={MineImg} />
            </BtnFocus>
          )
        : (
            <Btn onPress={props.MineIconOnPress}>
              <ImgStyle source={MineImg} />
            </Btn>
          )
      }
      {
        props.questionIconActivated
        ? (
            <BtnFocus onPress={props.QuestionMarkOnPress}>
              <ImgStyle source={QuesMarkImg} />
            </BtnFocus>
          )
        : (
            <Btn onPress={props.QuestionMarkOnPress}>
              <ImgStyle source={QuesMarkImg} />
            </Btn>
          )
      }


    </Container>
  )
}
