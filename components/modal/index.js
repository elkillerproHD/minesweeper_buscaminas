import React from 'react';
import {Text, TextInput, ScrollView} from 'react-native';
import styled from 'styled-components';
import {Constants} from '../../constants';
import {Close} from '../floatCircleBtn';

const SubContainer = styled.View`
  width: ${Constants.width}
  height: ${Constants.height * 1.2};
  background-color: #fdfdfd;
  align-items: center;
  justify-content: flex-start;
  padding-top: ${Constants.height * 0.035};
`;
const Button = styled.TouchableOpacity`
  width: ${Constants.width * 0.85};
  elevation: 1;
  height: ${Constants.height * 0.1};
  align-items: center;
  justify-content: center;
  background-color: #fff;
  margin-top: ${Constants.height * 0.035};
`;
const TextButtonContainer = styled.View`
  width: ${Constants.width * 0.75};
  height: ${Constants.height * 0.15};
  justify-content: center;
  align-items: flex-start;
`;

const TextInputComponents = {
  width: Constants.width * 0.85,
  elevation: 1,
  height: Constants.height * 0.09,
  alignItems: 'center',
  justifyContent: 'center',
  backgroundColor: '#fff',
  marginTop: Constants.height * 0.035,
  fontSize: 18,
  paddingLeft: Constants.width * 0.05,
  color: '#000',
};

const TextButton = {
  fontSize: 18,
  color: '#000',
};

const TextSection = {
  fontSize: 18,
  color: '#000',
  textAlign: 'left',
  width: Constants.width,
  paddingLeft: Constants.width * 0.075,
  marginTop: Constants.height * 0.035,
  marginBottom: Constants.height * 0.015 * -1,
};

export class Modal extends React.Component {
  render() {
    const {props} = this;
    // Alert.alert(props.colNumber.toString());
    return [
      <ScrollView
        style={{
          zIndex: 1,
          elevation: 1,
        }}
        keyboardShouldPersistTaps="handled"
        ref={(ref) => (this.ScrollRef = ref)}>
        <SubContainer>
          <Text style={TextSection}>Save/Load Game</Text>
          <Button onPress={() => {props.SaveGame()}}>
            <TextButtonContainer>
              <Text style={TextButton}>Save Game</Text>
            </TextButtonContainer>
          </Button>
          <Button onPress={() => {props.LoadGame()}}>
            <TextButtonContainer>
              <Text style={TextButton}>Load Game</Text>
            </TextButtonContainer>
          </Button>
          <Text style={TextSection}>Difficulty</Text>
          <Button onPress={() => {props.SetDifficulty("easy")}}>
            <TextButtonContainer>
              <Text style={TextButton}>Easy</Text>
            </TextButtonContainer>
          </Button>
          <Button onPress={() => {props.SetDifficulty("normal")}}>
            <TextButtonContainer>
              <Text style={TextButton}>Normal</Text>
            </TextButtonContainer>
          </Button>
          <Button onPress={() => {props.SetDifficulty("hard")}}>
            <TextButtonContainer>
              <Text style={TextButton}>Hard</Text>
            </TextButtonContainer>
          </Button>
          <Text style={TextSection}>Rows</Text>
          <TextInput
            style={TextInputComponents}
            onChangeText={(text) => props.OnChangeRow(text)}
            value={props.rowNumber.toString()}
            keyboardType={'number-pad'}
            editable={true}
          />
          <Text style={TextSection}>Columns</Text>
          <TextInput
            style={TextInputComponents}
            onChangeText={(text) => props.OnChangeCol(text)}
            value={props.colNumber.toString()}
            keyboardType={'number-pad'}
            editable={true}
          />
        </SubContainer>
      </ScrollView>,
      <Close onPress={props.OpenCloseModal} />,
    ];
  }
}
