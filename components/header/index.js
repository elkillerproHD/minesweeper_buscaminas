import React from 'react';
import styled from 'styled-components';
import {Constants} from '../../constants';
import SmileEmoji from '../../images/smile.png';
import DeadEmoji from '../../images/dead.png';

const Container = styled.View`
  width: ${Constants.width};
  height: ${Constants.height * 0.1};
  margin-top: ${Constants.height * 0.01};
  margin-bottom: ${Constants.height * 0.01};
  background-color: #bdbdbd;
  border-width: 8;
  border-bottom-color: #ffffff;
  border-right-color: #ffffff;
  border-left-color: #7d7d7d;
  border-top-color: #7d7d7d;
  justify-content: space-around;
  align-items: center;
  flex-direction: row;
`;

const DeadImageStContainer = styled.TouchableOpacity`
  width: ${Constants.height * 0.1 - 16};
  height: ${Constants.height * 0.1 - 16};
  background-color: #ff0000;
  border-width: 5;
  border-top-color: #ffffff;
  border-left-color: #ffffff;
  border-right-color: #7d7d7d;
  border-bottom-color: #7d7d7d;
  justify-content: center;
  align-items: center;
`;

const ImageStContainer = styled.TouchableOpacity`
  width: ${Constants.height * 0.1 - 16};
  height: ${Constants.height * 0.1 - 16};
  background-color: #bdbdbd;
  border-width: 5;
  border-top-color: #ffffff;
  border-left-color: #ffffff;
  border-right-color: #7d7d7d;
  border-bottom-color: #7d7d7d;
  justify-content: center;
  align-items: center;
`;
const ImageSt = styled.Image`
  width: ${Constants.height * 0.1 - 32};
  height: ${Constants.height * 0.1 - 32};
`;
const RedTextContainer = styled.View`
  padding-left: ${Constants.width * 0.05};
  padding-right: ${Constants.width * 0.05};
  min-width: ${Constants.width * 0.25};
  height: ${Constants.height * 0.1 - 28};
  background-color: #000;
  justify-content: center;
  align-items: center;
`;
const RedText = styled.Text`
  color: #ff0000;
  font-size: ${Constants.height * 0.1 - 28};
`;

export const Header = (props) => {
  if (props.gameOver && !props.winGame) {
    return (
      <Container>
        <RedTextContainer>
          <RedText>
            {(props.minesInGame - props.minesIconsUsedQuantity)
              .toString()
              .padStart(3, '0')}
          </RedText>
        </RedTextContainer>
        <DeadImageStContainer onPress={props.reset}>
          <ImageSt source={DeadEmoji} />
        </DeadImageStContainer>
        <RedTextContainer>
          <RedText>{props.timeLapse.toString().padStart(3, '0')}</RedText>
        </RedTextContainer>
      </Container>
    );
  } else {
    return (
      <Container>
        <RedTextContainer>
          <RedText>
            {(props.minesInGame - props.minesIconsUsedQuantity)
              .toString()
              .padStart(3, '0')}
          </RedText>
        </RedTextContainer>
        <ImageStContainer onPress={props.reset}>
          <ImageSt source={SmileEmoji} />
        </ImageStContainer>
        <RedTextContainer>
          <RedText>{props.timeLapse.toString().padStart(3, '0')}</RedText>
        </RedTextContainer>
      </Container>
    );
  }
};
