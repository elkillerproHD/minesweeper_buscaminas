import React from 'react';
import {Text, Image} from 'react-native';
import styled from 'styled-components';
import {Constants} from '../../constants';
import MineImage from '../../images/mina.png';
import MineIconHelpIcon from '../../images/mine.png';
import QuestionMarkIcon from '../../images/question.png';

const CellClosed = styled.TouchableOpacity`
  width: ${({zoom}) => Constants.cellSize * (1 + zoom / 10)};
  height: ${({zoom}) => Constants.cellSize * (1 + zoom / 10)};
  background-color: #bdbdbd;
  border-width: 3;
  border-top-color: #ffffff;
  border-left-color: #ffffff;
  border-right-color: #7d7d7d;
  border-bottom-color: #7d7d7d;
  justify-content: center;
  align-items: center;
`;
const CellOpened = styled.TouchableOpacity`
  width: ${({zoom}) => Constants.cellSize * (1 + zoom / 10)};
  height: ${({zoom}) => Constants.cellSize * (1 + zoom / 10)};
  background-color: #bdbdbd;
  border-width: 1;
  border-color: #7d7d7d;
  justify-content: center;
  align-items: center;
`;

export class Cell extends React.Component {
  constructor(props) {
    super(props);
  }

  onReveal = (userInitiated) => {
    if (!userInitiated && this.props.isMine) {
      return;
    }
    if (this.props.isMine) {
      this.props.RedMineCellOnPress(this.props.x, this.props.y);
    } else {
      this.props.onReveal(this.props.x, this.props.y);
    }
  };
  CellOnPress = () => {
    const {props} = this;
    if (this.props.gameOver) {
      return;
    }
    if (this.props.open) {
      return;
    }

    if (!props.mineIconActivated && !props.questionIconActivated) {
      this.onReveal(true);
    } else {
      if (props.mineIconActivated) {
        props.AddMineIconToCell(props.x, props.y, props.isMine);
      } else if (props.questionIconActivated) {
        props.AddQuestionMarkToCell(props.x, props.y);
      }
    }
  };

  render() {
    const {props} = this;
    let content = null;
    if (!props.open) {
      if (this.props.questionMarkIcon) {
        content = (
          <Image
            source={QuestionMarkIcon}
            style={{
              width: (Constants.cellSize * (1 + props.zoom / 10)) / 2,
              height: (Constants.cellSize * (1 + props.zoom / 10)) / 2,
            }}
            resizeMode="contain"
          />
        );
        return (
          <CellClosed zoom={props.zoom} onPress={() => this.CellOnPress()}>
            {content}
          </CellClosed>
        );
      } else if (this.props.mineIcon) {
        content = (
          <Image
            source={MineIconHelpIcon}
            style={{
              width: (Constants.cellSize * (1 + props.zoom / 10)) / 2,
              height: (Constants.cellSize * (1 + props.zoom / 10)) / 2,
            }}
            resizeMode="contain"
          />
        );
        return (
          <CellClosed zoom={props.zoom} onPress={() => this.CellOnPress()}>
            {content}
          </CellClosed>
        );
      }
      return (
        <CellClosed zoom={props.zoom} onPress={() => this.CellOnPress()} />
      );
    } else {
      if (props.isMine) {
        content = (
          <Image
            source={MineImage}
            style={{
              width: (Constants.cellSize * (1 + props.zoom / 10)) / 2,
              height: (Constants.cellSize * (1 + props.zoom / 10)) / 2,
            }}
            resizeMode="contain"
          />
        );
      } else if (this.props.neighbors) {
        let textColor = null;
        switch (this.props.neighbors) {
          case 1:
            textColor = '#080bec';
            break;
          case 2:
            textColor = '#008003';
            break;
          case 3:
            textColor = '#fe0001';
            break;
          case 4:
            textColor = '#fe0001';
            break;
          case 5:
            textColor = '#800100';
            break;
          case 6:
            textColor = '#017f83';
            break;
          case 7:
            textColor = '#000000';
            break;
          case 8:
            textColor = '#808080';
            break;
        }
        content = (
          <Text
            style={{
              fontSize: 16 * (1 + props.zoom / 10),
              fontWeight: 'bold',
              color: textColor
            }}>
            {this.props.neighbors}
          </Text>
        );
      }
      return (
        <CellOpened
          zoom={props.zoom}
          style={props.mineCellPressed && {backgroundColor: '#ff0000'}}>
          {content}
        </CellOpened>
      );
    }
  }
}
