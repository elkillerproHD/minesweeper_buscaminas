import React from 'react';
import Game from './containers/game';

const App = () => {
  return <Game />;
};

export default App;
