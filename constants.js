import { Dimensions } from 'react-native';

export const Constants = {
  cellSize: 25,
  width: Dimensions.get('screen').width,
  height: Dimensions.get('screen').height,
  isMineConstant: 0.15,
  maxZoom: 35,
  zoomAddedLvl: 5,
  hardcodeRow: 10,
  hardcodeCol: 10,
}
